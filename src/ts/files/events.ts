
import { addMoney, checkRemoveAddClass } from './functions';
import { startData } from './startData';
import { configWormGame, startGame, stopGame, resetGame } from './script';

const wormHead = document.querySelector('.worm__head');

// Объявляем слушатель событий "клик"
document.addEventListener('click', (e) => {

	const wrapper = document.querySelector('.wrapper');

	const targetElement = e.target as HTMLElement;

	const money = Number(localStorage.getItem('money'));
	const bet = Number(localStorage.getItem('current-bet'));

	// privacy screen
	if (targetElement.closest('.preloader__button')) {
		location.href = 'main.html';
	}

	// main screen
	if (targetElement.closest('[data-button="privacy"]')) {
		location.href = 'index.html';
	}

	if (targetElement.closest('[data-button="game"') && configWormGame.wormGame && configWormGame.wormGame.worm) {
		wrapper?.classList.add('_game');
		configWormGame.wormGame.createFruits();
		configWormGame.wormGame.worm.createStartWormLinks();
		setTimeout(() => {
			startGame();
		}, 250);
	}

	if (targetElement.closest('[data-button="game-home"')) {
		wrapper?.setAttribute('class', 'wrapper');
		stopGame();
		setTimeout(() => {
			resetGame();
		}, 600);
	}

	if (targetElement.closest('.field__body') && configWormGame.state === 2) {
		const xTouch = e.x;
		const yTouch = e.y;

		if (wormHead && configWormGame.wormGame && configWormGame.wormGame.worm) {
			const xHead = wormHead.getBoundingClientRect().x;
			const yHead = wormHead.getBoundingClientRect().y;
			const wormSize = wormHead.getBoundingClientRect().width;

			// если кликнули левее головы и нужно проверить что y головы выше y хвоста минимум на размер одного звена
			if (configWormGame.wormGame.currentDirection === 'up' || configWormGame.wormGame.currentDirection === 'down') {
				if (xTouch < xHead) {
					configWormGame.wormGame.currentDirection = 'left';
					configWormGame.wormGame.worm.rotate = 180;
				} else if (xTouch > xHead) {
					configWormGame.wormGame.currentDirection = 'right';
					configWormGame.wormGame.worm.rotate = 0;
				}
			} else if (configWormGame.wormGame.currentDirection === 'left' || configWormGame.wormGame.currentDirection === 'right') {
				if (yTouch < yHead) {
					// меняем направление на вверх
					configWormGame.wormGame.currentDirection = 'up';
					configWormGame.wormGame.worm.rotate = 270;
				} else if (yTouch > yHead) {
					// меняем направление на вверх
					configWormGame.wormGame.currentDirection = 'down';
					configWormGame.wormGame.worm.rotate = 90;
				}
			}



		}
	}

})
